<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Users;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\MessageBag;

use Illuminate\Support\Facades\Hash;

use Validator;

class UserController extends Controller
{

    public function SuccessResponse($result, $message){

        $response = [
            'success' => true,
            'message' => $message,
            'data' => $result,     
        ];

        return response()->json($response, 200);
    }

    public function ErrorResponse($error, $errorMessages = [], $code = 404){

        $response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    public function All() {

        $user = Users::all();

        return $this->SuccessResponse($user->toArray(), 'User retrieved successfully');
    }

    public function ById(Request $request) {

        $user = Users::find($request->id);

        if ($user === null) {            
            return $this->ErrorResponse('Data is not exist');
        }

        return $this->SuccessResponse($user, 'User is exist');

    }

    public function Register(Request $request) {

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return $this->ErrorResponse('Validation Error', $validator->errors());
        }
    
        $user = Users::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return $this->SuccessResponse($user, 'Successfully register' );
    }   

    public function Update(Request $request) {

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return $this->ErrorResponse('Validation Error', $validator->errors());
        }

        $user = Users::where('id', $request->id)
                ->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)
                ]);

        return $this->SuccessResponse($user, 'Update is success.' );
    }

    public function Delete(Request $request) {

        $user = Users::destroy($request->id);

        if (!$user) {
            return $this->ErrorResponse('Data is not exist!');
        }

        return response()->json('Data is successfully deleted!');

    }
}
